package com.guz.graphql.data;

import org.springframework.data.annotation.Id;

import java.util.Objects;

public class PayRolDocument {
    @Id
    private String id;

    private String name, data, bases, iess, thirteenth, fourteenth, background, tpay;

    private boolean salario;

    public PayRolDocument() {
    }

    public PayRolDocument(String id, String name, String data, String bases, String iess, String thirteenth, String fourteenth, String background, String tpay, boolean salario) {
        this.id = id;
        this.name = name;
        this.data = data;
        this.bases = bases;
        this.iess = iess;
        this.thirteenth = thirteenth;
        this.fourteenth = fourteenth;
        this.background = background;
        this.tpay = tpay;
        this.salario = salario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getBases() {
        return bases;
    }

    public void setBases(String bases) {
        this.bases = bases;
    }

    public String getIess() {
        return iess;
    }

    public void setIess(String iess) {
        this.iess = iess;
    }

    public String getThirteenth() {
        return thirteenth;
    }

    public void setThirteenth(String thirteenth) {
        this.thirteenth = thirteenth;
    }

    public String getFourteenth() {
        return fourteenth;
    }

    public void setFourteenth(String fourteenth) {
        this.fourteenth = fourteenth;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getTpay() {
        return tpay;
    }

    public void setTpay(String tpay) {
        this.tpay = tpay;
    }

    public boolean isSalario() {
        return salario;
    }

    public void setSalario(boolean salario) {
        this.salario = salario;
    }
}
