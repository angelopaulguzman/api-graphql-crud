package com.guz.graphql.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collation = "pay")
public class PayDocument {
    @Id
    private String id;

    private String name;
    private Float salario;

    public PayDocument() {
    }

    public PayDocument(String id, String name, Float salario) {
        this.id = id;
        this.name = name;
        this.salario = salario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PayDocument that = (PayDocument) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(salario, that.salario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, salario);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getSalario() {
        return salario;
    }

    public void setSalario(Float salario) {
        this.salario = salario;
    }
}