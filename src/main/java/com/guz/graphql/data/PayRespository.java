package com.guz.graphql.data;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PayRespository extends MongoRepository<PayDocument, String> {
}
