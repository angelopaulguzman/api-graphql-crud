package com.guz.graphql.data;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PayRolRepository extends MongoRepository<PayRolDocument, String> {
}
