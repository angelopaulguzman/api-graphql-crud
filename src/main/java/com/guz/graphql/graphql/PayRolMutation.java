package com.guz.graphql.graphql;

import com.guz.graphql.data.PayRolDocument;
import com.guz.graphql.services.PayRolService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class PayRolMutation implements GraphQLMutationResolver {

    private PayRolService payRolService;

    @Autowired
    public PayRolMutation(PayRolService payRolService){
        this.payRolService = payRolService;
    }

    public PayRolDocument createCar(PayRolDocument payRolDocument){
        return this.payRolService.createCar(payRolDocument);
    }

    public PayRolDocument updateCar(String id, PayRolDocument payRolDocument){
        return this.payRolService.updateCarId(id, payRolDocument);
    }

    public PayRolDocument deleteCar(String id){
        return this.payRolService.deleteCarById(id);
    }
}
