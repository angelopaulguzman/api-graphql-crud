package com.guz.graphql.graphql;

import com.guz.graphql.data.PayRolDocument;
import com.guz.graphql.services.PayRolService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class PayRolQuery implements GraphQLQueryResolver {
    private PayRolService payRolService;

    @Autowired
    public PayRolQuery(PayRolService payRolService){
        this.payRolService = payRolService;
    }

    public List<PayRolDocument> car(){
        return this.payRolService.getAllCars();
    }

      public PayRolDocument car(String id) {
        return this.payRolService.getCarById(id);
    }





}
