package com.guz.graphql.services;

import com.guz.graphql.data.PayRolDocument;
import com.guz.graphql.data.PayRolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PayRolService {
    private PayRolRepository payRolRepository;

    @Autowired
    public PayRolService(PayRolRepository payRolRepository) {
        this.payRolRepository = payRolRepository;
    }

    public List<PayRolDocument> getAllCars(){
        return this.payRolRepository.findAll();
    }

    public PayRolDocument createCar(PayRolDocument payRolDocument) {
        return this.payRolRepository.save(payRolDocument);
    }

    public PayRolDocument getCarById(String id){
        Optional<PayRolDocument> carDocumentOptional = this.payRolRepository.findById(id);
        return carDocumentOptional.get();
    }

    public PayRolDocument updateCarId(String id, PayRolDocument payRolDocument){
        Optional<PayRolDocument> carDocumentOptional = this.payRolRepository.findById(id);
        PayRolDocument carDb = carDocumentOptional.get();

        return this.payRolRepository.save(carDb);
    }

    public PayRolDocument deleteCarById(String id){
        Optional<PayRolDocument> carDocumentOptional = this.payRolRepository.findById(id);
        PayRolDocument carDb = carDocumentOptional.get();
        this.payRolRepository.deleteById(id);
        return carDb;
    }
}
